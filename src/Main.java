import co.fingerprintsoft.transport.Plane;

import java.util.random.RandomGenerator;
import java.util.random.RandomGeneratorFactory;

public class Main {

    public static void main(String... args) {
        Plane plane = new Plane();
        System.out.println(plane);

        System.out.println(formatValue(10.5));
        System.out.println(formatValue(5));
        System.out.println(formatValue(420L));
        System.out.println(formatValue("java 17"));

        System.out.println(formatNonEmptyString("string"));
        System.out.println(formatNonEmptyString(""));


        System.out.println(formatValidString("zz"));
        System.out.println(formatValidString("!"));
        System.out.println(formatValidString("@@"));

        formatNullAndSpecialValues(null);
        formatNullAndSpecialValues("Java");
        formatNullAndSpecialValues("Foo Bar");

        System.out.println(headsOrTails());
        System.out.println(diceRoll());
    }


    // Type Patterns
    static String formatValue(Object o) {
        return switch(o) {
            case Double d   -> String.format("Double value is %f", d);
            case Integer i  -> String.format("Integer value is %d", i);
            case Long l     -> String.format("Long value is %d", l);
            case String s   -> String.format("String value is %s", s);
            default         -> o.toString();
        };
    }

    // Guarded Patterns
    static String formatNonEmptyString(Object o) {
        return switch(o) {
            case String s && s.length() >= 1    -> String.format("String value is %s", s);
            case String s                       -> String.format("Empty string");
            default                             -> o.toString();
        };
    }

    // Parenthesized Patterns
    static String formatValidString(Object o) {
        return switch(o) {
            case String s && s.length() >= 2 && (s.contains("@") || s.contains("!")) -> String.format("String value is %s", s);
            default                                                                  -> o.toString();
        };
    }

    // Null Values Handling
    static void formatNullAndSpecialValues(String s) {
        switch(s) {
            case null                            -> System.out.println("Value is null");
            case "Fingerprints Software", "Java" -> System.out.println("Special value");
            default                              -> System.out.println("Other value");
        }
    }

    static String headsOrTails() {
        boolean coinToss = RandomGenerator.of("L128X1024MixRandom").nextBoolean();
        if (coinToss) {
            return "Heads";
        }
        return "Tails";
    }

    static int diceRoll() {
        return RandomGenerator.of("Xoroshiro128PlusPlus").nextInt(6) + 1;
    }
}
