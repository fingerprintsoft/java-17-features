package co.fingerprintsoft.transport;

public final class Car implements Vehicle {

    @Override
    public String toString() {
        return name();
    }

    @Override
    public int weight() {
        return 100;
    }

    @Override
    public String name() {
        return "car";
    }

    @Override
    public int wheels() {
        return 4;
    }
}
