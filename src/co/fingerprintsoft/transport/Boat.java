package co.fingerprintsoft.transport;

public final class Boat implements Vehicle {

    @Override
    public String toString() {
        return name();
    }

    @Override
    public int weight() {
        return 1000;
    }

    @Override
    public String name() {
        return "boat";
    }

    @Override
    public int wheels() {
        return 0;
    }
}
