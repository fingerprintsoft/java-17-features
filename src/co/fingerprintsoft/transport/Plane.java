package co.fingerprintsoft.transport;

public final class Plane implements Vehicle {

    @Override
    public String toString() {
        return name();
    }

    @Override
    public int weight() {
        return 10000;
    }

    @Override
    public String name() {
        return "plane";
    }

    @Override
    public int wheels() {
        return 2;
    }
}
