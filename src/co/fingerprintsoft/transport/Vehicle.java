package co.fingerprintsoft.transport;

public sealed interface Vehicle
    permits Plane, Car, Boat {

    int weight();

    String name();

    int wheels();
}
